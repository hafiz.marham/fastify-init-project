**FASTIFY BACKEND API by hafizmarham**

# Fastify Init Project
It is repository that you can use to init a fastify API project.
You can use it as basic fastify API project which use Fastify(NodeJS Framework) and PostgreSQL(as database).
Clone this project to your own repo, and you can rename it as you want. 
> ex: 'my-backend-api' or something else

## Installation
Make sure you are already install the node js.

Run this command below to init a NodeJS project for this repository.
```
npm install
```

Create .env file, and write this on it:
```
DB_HOST = "localhost or your_db_host"
DB_USER = "your_db_user"
DB_PASSWORD = "your_db_password"
DB_NAME = "your_db_name"
DB_PORT = "your_db_port"
 
APP_PORT = this_app_port
```

## Folder Root
```
> node_modules/
> router/
>    - your router dir/
>        - your_router_dir.js
>    - your_router.js
> .env
> .gitignore
> index.js
> package-lock.json
> package.json
> README.md
```

## Usage

Run this app with the command below
```
nodemon start
```

If this app did not run or some errors occurs,
Make sure all depedencies are already install, which are:
- fastify
- fastify-postgres -> you can change it if you want another database
- fastify-route-group
- fastify-swagger -> it use just to check your api run or not
- nodemon
- dotenv

## Contributing
Pull requests are welcome.
It made just because i feel so bored ahahahah *lol

## Contact
hafizmarham / damarham13@gmail.com