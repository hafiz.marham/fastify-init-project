async function secondRouter (fastify, options) {
    const { Router } = require('fastify-route-group');

    const router = new Router(fastify);
    
    router.namespace('second', () => {
        //get api '' or second/
        router.get('/', async (request, reply) => {
            fastify.pg.connect(onConnect) //call the postgre DB

            function onConnect ( err, client, release ) {
                if ( err ) return reply.send( err )

                client.query(
                    'SELECT email FROM dtc.user',
                    function onResult ( err, result ) {
                        release()
                        reply.send(err || result)
                    }
                )
            }
        });
        
        //get api second/old
        router.get('/old', async (request, reply) => {
            reply.send('ITS THE SECOND OLD ONE');
        });
    });
}

module.exports = secondRouter