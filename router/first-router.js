async function firstRouter (fastify, options) {
    const { Router } = require('fastify-route-group');

    const router = new Router(fastify);
    
    router.namespace('first', () => {
        //get api '' or first/
        router.get('/', async (request, reply) => {
            fastify.pg.connect(onConnect)

            function onConnect ( err, client, release ) {
                if ( err ) return reply.send( err )

                client.query(
                    'SELECT email FROM dtc.user',
                    function onResult ( err, result ) {
                        release()
                        reply.send(err || result)
                    }
                )
            }

            // reply.send('WELCOME TO THE FIRST WORLD');
        });
        
        //get api first/new
        router.get('/new', async (request, reply) => {
            reply.send('MAKE NEW ONE FIRST API');
        });
    });
}

module.exports = firstRouter