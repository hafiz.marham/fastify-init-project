// ========================IMPORT=OPEN=TAG=======================
const fastify = require('fastify')({ // init fastify
    logger: true
})

require('dotenv').config() //import dotenv
const path = require('path') //import path
const fs = require('fs') //read file
// ========================IMPORT=CLOSE=TAG======================


// ========================DB=OPEN=TAG=======================
fastify.register(require('fastify-postgres'), {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD
})
// ========================DB=CLOSE=TAG======================


// ========================SWAGGER=OPEN=TAG=======================
fastify.register(require('fastify-swagger'), {
    routePrefix: '/documentation',
    swagger: {
        info: {
            title: 'Test swagger',
            description: 'Testing the Fastify swagger API',
            version: '0.1.0'
        },
        externalDocs: {
            url: 'https://swagger.io',
            description: 'Find more info here'
        },
        host: '127.0.0.1:'+process.env.APP_PORT,
        schemes: ['http'],
        consumes: ['application/json'],
        produces: ['application/json'],
        definitions: {
            User: {
                type: 'object',
                required: ['id', 'email'],
                properties: {
                    id: { type: 'string', format: 'uuid' },
                    firstName: { type: 'string' },
                    lastName: { type: 'string' },
                    email: {type: 'string', format: 'email' }
                }
            }
        },
        securityDefinitions: {
            apiKey: {
            type: 'apiKey',
            name: 'apiKey',
            in: 'header'
            }
        }
    },
    uiConfig: {
        docExpansion: 'full',
        deepLinking: false
    },
    uiHooks: {
        onRequest: function (request, reply, next) { next() },
        preHandler: function (request, reply, next) { next() }
    },
    staticCSP: true,
    transformStaticCSP: (header) => header,
    exposeRoute: true
})
// ========================SWAGGER=CLOSE=TAG======================


// ========================ROUTER=OPEN=TAG=======================
// router default
fastify.get('/', async (request, reply) => {
    return 'Welcome to your Fastify project, this is your fastify "host:port/" default router'
})

// read router folder or file
function Dig(_path) {
    fs.readdirSync(_path).forEach(file => {
        const fullpath = path.join(_path, file);
    
        const stats = fs.statSync(fullpath);
        if (stats.isDirectory()) {
            // console.log('Dig deep');
            Dig(fullpath);
        } else if (stats.isFile()) {
            // console.log('Include');
            fastify.register(require(fullpath))
        } else {
            console.log(fullpath,' Unknown ');
        }
      });
}
const pathrouters = path.join(__dirname, 'router');
Dig(pathrouters);
// ========================ROUTER=CLOSE=TAG======================


// ========================RUN THE SERVER APPS=OPEN=TAG===========
const start = async () => {
    try {
      await fastify.listen(process.env.APP_PORT)
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
}
start()
// ========================RUN THE SERVER APPS=CLOSE=TAG==========